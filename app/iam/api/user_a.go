package api

type CreateUserRequest struct {
	UserName    string `json:"userName" binding:"required"`
	Password    string `json:"password" binding:"required"`
	DisplayName string `json:"displayName" binding:"required"`
}

type CreateUserResponse struct {
	UserName string `json:"userName"`
	Message  string `json:"message"`
}

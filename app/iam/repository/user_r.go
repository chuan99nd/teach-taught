package repository

import (
	"chuan.io/teach-taught/app/iam/model"
	"gorm.io/gorm"
)

type UserRepository struct {
	DB        *gorm.DB
	TableName string
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{db, "user"}
}

func (s *UserRepository) Create(user *model.User) error {
	err := s.DB.Table(s.TableName).Create(user).Error
	return err
}

func (s *UserRepository) GetByUserName(userName string) (*model.User, error) {
	user := &model.User{}
	err := s.DB.Table(s.TableName).Where("user_name = ?", userName).First(&user).Error
	return user, err
}

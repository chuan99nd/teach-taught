package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const BearScheme = "Beared:"

func Authenticator(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if len(token) < len(BearScheme) {
		c.JSON(http.StatusNonAuthoritativeInfo, "Invalid token")
		return
	}
	token = token[len(BearScheme):]
	// fmt.Print(token)
}

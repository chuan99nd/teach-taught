package handler

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/http"

	"chuan.io/teach-taught/app/iam/api"
	"chuan.io/teach-taught/app/iam/model"
	"chuan.io/teach-taught/app/iam/utils"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

const (
	SaltLength = 10
)

func (h *Handler) CreateUser(ctx *gin.Context) {
	req := api.CreateUserRequest{}
	// res := CreateUserResponse{}
	err := ctx.Bind(&req)
	if err != nil {
		ctx.JSON(http.StatusNoContent, "Chưa điền tên đăng nhập hoăc mật khẩu")
		return
	}
	user, err := h.UserRepository.GetByUserName(req.UserName)
	if user.Id > 0 {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("Username %s đã tồn tại", req.UserName))
		return
	}
	if err != gorm.ErrRecordNotFound {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	pass := req.Password
	salt := utils.RandString(SaltLength)
	sum := md5.Sum([]byte(pass + salt))
	passwordHash := hex.EncodeToString(sum[:])
	err = h.UserRepository.Create(&model.User{
		UserName:     req.UserName,
		PasswordHash: passwordHash,
		Salt:         salt,
		DisplayName:  req.DisplayName,
	})
	if err != nil {
		ctx.JSON(http.StatusBadGateway, err.Error())
		return
	}
	res := api.CreateUserResponse{
		UserName: req.UserName,
		Message:  "OK",
	}
	ctx.JSON(http.StatusOK, res)
}

package handler

import (
	"chuan.io/teach-taught/app/iam/repository"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	UserRepository *repository.UserRepository
}

func (h *Handler) RetgisterRouter() *gin.Engine {
	router := gin.Default()
	router.POST("/sign-up", h.CreateUser)
	api := router.Group("/api")
	api.Use(Authenticator)
	{
		api.POST("/get-user-info")
	}
	return router
}

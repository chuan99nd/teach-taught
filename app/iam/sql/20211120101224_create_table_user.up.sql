CREATE Table `user` (
    `id` INT  AUTO_INCREMENT not null,
    `user_name` VARCHAR(63) UNIQUE not null COMMENT '',
    `display_name` VARCHAR(63) not null COMMENT 'Name to display',
    `password_hash` VARCHAR(255) COMMENT '',
    `salt` VARCHAR(63) COMMENT '',
    primary key (id)
);

Create index id_user_name on `user` (user_name); 
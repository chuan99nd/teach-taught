package main

import (
	"os"

	"chuan.io/teach-taught/app/iam/handler"
	"chuan.io/teach-taught/app/iam/repository"
	"chuan.io/teach-taught/library/database"
	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
)

func main() {
	env, err := godotenv.Read()
	if err != nil {
		panic(err)
	}
	mySqlConfig := database.GetSqlConfigFromEnv()
	app := cli.App{
		EnableBashCompletion: true,
		Authors: []*cli.Author{
			{
				Name:  "Tran Hoang Chuan dz",
				Email: "chuannd1999@gmail.com",
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "service",
				Aliases: []string{"s"},
				Usage:   "run service",
				Action:  runService,
			}, {
				Name:        "migrate",
				Aliases:     []string{"m"},
				Usage:       "migrate database",
				Subcommands: database.GetCliCommand(env["MigrateFolder"], mySqlConfig.DbConnection()),
			},
		},
	}
	err = app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runService(c *cli.Context) error {
	env, err := godotenv.Read()
	if err != nil {
		panic(err)
	}
	sqlConfig := database.GetSqlConfigFromEnv()
	db := sqlConfig.NewDb()
	userRepo := repository.NewUserRepository(db)
	handler := handler.Handler{
		UserRepository: userRepo,
	}
	r := handler.RetgisterRouter()
	err = r.Run(env["IamPortHost"])
	if err != nil {
		panic("Error when bind service")
	}
	return nil
}

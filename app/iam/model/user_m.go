package model

type User struct {
	Id           int32
	UserName     string
	PasswordHash string
	Salt         string
	DisplayName  string
}

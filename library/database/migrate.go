package database

import (
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	migrate "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pingcap/log"
	"github.com/urfave/cli/v2"
)

func GetCliCommand(sourceFolder string, connectionString string) []*cli.Command {
	return []*cli.Command{
		{
			Name:  "up",
			Usage: "update database",
			Action: func(c *cli.Context) error {
				m, err := migrate.New(sourceFolder, connectionString)
				if err != nil {
					panic(err)
				}
				log.Info("Start migrate up")
				if err = m.Up(); err != nil {
					panic(err)
				}
				return nil
			},
		}, {
			Name:  "down",
			Usage: "downdate database",
			Action: func(c *cli.Context) error {
				m, err := migrate.New(sourceFolder, connectionString)
				if err != nil {
					panic(err)
				}
				log.Info("Start migrate down")
				if err = m.Steps(-1); err != nil {
					panic(err)
				}
				return nil
			},
		}, {
			Name:  "new",
			Usage: "create new migrate",
			Action: func(c *cli.Context) error {
				folder := strings.ReplaceAll(sourceFolder, "file://", "")
				now := time.Now()
				ver := now.Format("20060102150405")
				name := strings.Join(c.Args().Slice(), "-")

				up := fmt.Sprintf("%s/%s_%s.up.sql", folder, ver, name)
				down := fmt.Sprintf("%s/%s_%s.down.sql", folder, ver, name)

				if err := ioutil.WriteFile(up, []byte{}, 0600); err != nil {
					log.Fatal("Create migration up error")
				}
				if err := ioutil.WriteFile(down, []byte{}, 0600); err != nil {
					log.Fatal("Create migration down error")
				}
				return nil
			},
		},
	}
}

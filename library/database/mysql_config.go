package database

import (
	"fmt"
	"net/url"

	"github.com/joho/godotenv"
	gormSql "gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type MySqlConfig struct {
	Host         string
	Port         string
	UserName     string
	Password     string
	DatabaseName string
}

func GetSqlConfigFromEnv() *MySqlConfig {
	env, err := godotenv.Read()
	if err != nil {
		panic(err)
	}
	return &MySqlConfig{
		Host:         env["DBHost"],
		Port:         env["DBPort"],
		UserName:     env["DBUserName"],
		Password:     env["DBPassword"],
		DatabaseName: env["DBDatabaseName"],
	}
}
func (m *MySqlConfig) DSN() string {
	DSN := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		m.UserName,
		url.QueryEscape(m.Password),
		m.Host,
		m.Port,
		m.DatabaseName)
	return DSN
}

func (m *MySqlConfig) DbConnection() string {
	return fmt.Sprintf("mysql://%s", m.DSN())
}

func (m *MySqlConfig) NewDb() *gorm.DB {
	// Init db
	DSN := m.DSN()
	db, err := gorm.Open(gormSql.Open(DSN), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	return db
}
